from flask import jsonify, request
from app import app
import requests


@app.route('/monitors', methods=['POST'])
def monitors():
    try:
        monitor_urls = dict()
        u_key = "user"
        p_key = "pass"
        u_val = request.form[u_key]
        p_val = request.form[p_key]
        post_params = {u_key: u_val, p_key: p_val}
        response = requests.post("http://10.10.0.100/zm/api/host/login.json", params=post_params)
        custom_header = {"Cookie": "ZMSESSID"+"="+response.cookies["ZMSESSID"]}
        response = requests.get("http://10.10.0.100/zm/api/monitors.json", headers=custom_header)
        for item in response.json()["monitors"]:
            item = item["Monitor"]
            if item["Name"]:
                monitor_urls[item["Name"]] = item["Path"]

        return jsonify(monitor_urls), 200
    except:
        return "", 404
